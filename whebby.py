# whebby.py - Andrew Jenkins 2020
# A virtual CUSAGC mascot for these unprecedented and troubling times.
# Uses discord.py. Must have a login token in the file `token.txt'

# Inspired by example at https://discordpy.readthedocs.io/en/latest/quickstart.html#a-minimal-bot

import discord
import string
import time

CUSAGC_WORDS = ["cusagc", "cambridge", "duck", "whimsy", "whispy", "whebby", "whittea"]
NICK = "Whebby the Duck"
COMMAND_PREFIX = "$whebby."

# so that commands within the text don't trigger his recognition of his own name.
punctuation = "".join([i for i in string.punctuation if i != "$"])
print("Punctuation: {}".format(punctuation))

# announce a committee speaker only if it is this more than this many seconds
# since we last saw them write something.
ADMIRATION_SILENCE_SECONDS = 3600

# list of current commands & description for the help function - please update when you add things!
Commands = {"stop":"I go away :(", "friendme":"I'll make you my friend :)", "unfriendme":"now I'm not your friend :(", "friends":"I'll tell you who all my friends are!", "about":"I'll introduce myself", "steal":"don't be so mean!", "help":"surely you know this one?"}

class CommitteeMember:
    
    def __init__(self, role):
        self.role = role
        self.last_spoke = 0

class WhebbyDuck(discord.Client):

    def __init__(self, committee, friends, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.committee = committee
        # friends is a set of ids for friends.
        self.friends = friends
    
    def add_friend(self, member):
        self.friends.add(member.id)
    
    def remove_friend(self, member):
        if self.is_friend(member):
            self.friends.remove(member.id)

    def is_friend(self, member):
        return member.id in self.friends
    
    async def add_friend_interactive(self, member, channel):
        if self.is_friend(member):
            await channel.send("I'm already friends with you, {}".format(member.mention))
        else:
            self.add_friend(member)
            await channel.send("Thanks {}, you're now my friend <3".format(member.mention))
    
    async def about_me(self, channel):
        await channel.send("I'm Whebby, a digital duck mascot for the Cambridge University Scout And Guide Club (:CUSAGC: CUSAGC). Try to steal me, I dare you.")
    
    async def steal(self, member, channel):
        if self.is_friend(member):
            self.remove_friend(member)
            await channel.send("With friends like these, who needs enemies? You're not my friend any more, {}.".format(member.mention))
        else:
            await channel.send("As {} tries to grab me, I *duck* for cover.".format(member.mention))
    
    async def remove_friend_interactive(self, member, channel):
        if self.is_friend(member):
            self.remove_friend(member)
            await channel.send("I'll miss you, {}".format(member.mention))
        else:
            await channel.send("We weren't even friends anyway, {} :angry:".format(member.mention))

    async def list_friends(self, channel):
        friend_mentions = []
        for member in channel.members:
            if self.is_friend(member):
                friend_mentions.append(member.mention)

        #put number of friends into variable to avoid calculating multiple times
        l = len(friend_mentions)
        if l == 0:
            await channel.send("I have no friends :((")
            return
        # add 'and' to printing out the names of friends if necessary
        elif l >= 2:
            friend_mentions = friend_mentions.insert(l-2,"and")
        await channel.send("My friends are: "+ " ".join(friend_mentions))
    
    # Help function
    async def display_help(self, channel):
        help_text = "Hi I'm Whebby, a digital duck mascot for the Cambridge University Scout And Guide Club (:CUSAGC: CUSAGC) :)\nLet me talk you through all the things I can currently do!\n\n"

        help_text += "```Commands:\n"
        for command in Commands:
            help_text += COMMAND_PREFIX + command + "  -  " + Commands[command] + "\n"
        
        # please update when you add new features :)
        help_text += "\nI always appreciate it when I hear from my committee members :)\n"        
        help_text += "I can also do fun stuff when you @ me in a message or mention some of my favourite things\n"
        help_text += "Try asking 'help', 'can I be your friend', 'who are your friends', 'who is your friend' or 'who are you' when you @ me in a message\n"
        help_text += "You can also attempt to catch or steal when you @ me, but it might not end well...```"

        await channel.send(help_text)
    
    async def on_ready(self):
        print('Logged in as {}'.format(self.user))
        print("I am member of {} guild(s):".format(len(self.guilds)))
        for g in self.guilds:
            print("   ", g)
            if g.me.nick != NICK:
                await g.me.edit(nick=NICK)
                print("Changed my name in {} to {}".format(g, NICK))
    
            print("Looking for committee...")
            for name in self.committee:
                if g.get_member_named(name) is not None:
                    print("Found {}, our {}".format(name, self.committee[name].role))

    async def on_message(self, message):
        if message.author == self.user:
            # ignore own messages.
            return
        
        if message.content.startswith(COMMAND_PREFIX):
            # commands
            command = message.content[len(COMMAND_PREFIX):]
            if command == "stop":
                print("Stopped.")
                await self.close()
            elif command == "friendme":
                await self.add_friend_interactive(message.author, message.channel)
            elif command == "unfriendme":
                await self.remove_friend_interactive(message.author, message.channel)
            elif command == "friends":
                await self.list_friends(message.channel)
            elif command == "about":
                await self.about_me(message.channel)
            elif command == "steal":
                await self.steal(message.author, message.channel)
            elif command == "help":
                await self.display_help(message.channel)
            else:
                await message.channel.send("Are you on quack? I didn't understand that.")
        
        else:
            # normal messages
            author_name = str(message.author)
            content_lowercase = message.content.lower()
            # don't chop punctuation out of the middle of a word.
            words = [w.lstrip(punctuation).rstrip(punctuation) for w in message.content.lower().split(" ") if w.strip() != ""]
            
            if any(word in CUSAGC_WORDS for word in words):
                await message.channel.send("Quack!")
            
            elif self.user.mentioned_in(message):
                if len(words) == 2 and "catch" in words:
                    # only try to catch it if there were just two words, one of
                    # which was catch.
                    await message.channel.send("I am but a digital duck and cannot catch.")
                elif "can i be your friend" in content_lowercase:
                    await self.add_friend_interactive(message.author, message.channel)
                elif "who are your friends" in content_lowercase or "who is your friend" in content_lowercase:
                    await self.list_friends(message.channel)
                elif "who are you" in content_lowercase:
                    await self.about_me(message.channel)
                elif "steal" in words:
                    await self.steal(message.author, message.channel)
                elif "help" in words:
                    await self.display_help(message.channel)
                else:
                    await message.channel.send("I was mentioned <3")
                    
            elif author_name in self.committee:
                now = time.time()
                committee_member = self.committee[author_name]
                if now - committee_member.last_spoke > ADMIRATION_SILENCE_SECONDS:
                    await message.channel.send("Our beloved {} speaks!".format(committee_member.role))
                committee_member.last_spoke = now

if __name__ == "__main__":
    # now load the token from file.
    with open("token.txt", "r") as f:
        token = f.read()

    # committee.txt should be a text file with the role and discord username
    # separated by tabs.
    committee = {}
    try:
        with open("committee.txt") as c:
            for line in c:
                role, name = line.strip().split("\t")
                committee[name] = CommitteeMember(role)
    except FileNotFoundError:
        pass
        
    # friends.txt is a simple newline-separated list of ids we're friends with.
    friends = set()
    try:
        with open("friends.txt") as f:
            for line in f:
                user_id = line.strip()
                friends.add(int(user_id))
    except FileNotFoundError:
        # it's non-essential so ignore it if it doesn't exist.
        pass

    try:
        my_duck = WhebbyDuck(committee, friends)
        my_duck.run(token)
    finally:
        # end by saving our friend ids, for persistence between runs.
        with open("friends.txt", "w") as f:
            for friend in my_duck.friends:
                f.write(str(friend) + "\n")
